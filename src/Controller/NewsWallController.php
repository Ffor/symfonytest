<?php

namespace App\Controller;

use App\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsWallController extends AbstractController
{

    private $repository;

    /**
     * @Route("/", name="news_wall")
     */
    public function index()
    {

        $this->repository = $this->getDoctrine()->getRepository(News::class);

        $news = $this->repository->findAll();

        return $this->render('news_wall/index.html.twig', [
            'controller_name' => 'NewsWallController',
            'news' => $news
        ]);
    }
}
