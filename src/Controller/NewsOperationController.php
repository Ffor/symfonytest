<?php

namespace App\Controller;

use App\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsOperationController extends AbstractController
{
    /**
     * @Route("/news/operation", name="news_operation")
     */
    public function index()
    {
        return $this->render('news_operation/index.html.twig', [
            'controller_name' => 'NewsOperationController',
        ]);
    }

    /**
     * @Route("/news/operation/delete/{id}", methods={"GET","POST"}, name="delete_news")
     * @param $id
     * @return RedirectResponse
     */
    public function deleteNews($id){
        if(isset($id) && $id !== 0){
            $repository = $this->getDoctrine()->getRepository(News::class);
            $entityManager = $this->getDoctrine()->getManager();
            $news = $repository->find($id);
            $entityManager->remove($news);
            $entityManager->flush();
            return $this->redirectToRoute('news_wall');
        }
    }

    public function editNews(){

    }


    /**
     * @Route("/news/operation/add", methods={"GET","POST"}, name="add_news")
     * @param Request $request
     * @return JsonResponse
     */
    public function addNews(Request $request){
        $entityManager = $this->getDoctrine()->getManager();

        $news = new News();

        if(isset($request->request))
        {
            $news->setText($request->request->get('_text'));
            $news->setTitle($request->request->get('_title'));
            $entityManager->persist($news);
            $entityManager->flush();

            return new JsonResponse(array(
                'status' => 'OK',
                'message' => 'Create new news with ajax'
            ));
        }
        else
        {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'News is not created'
            ));
        }
    }
}
