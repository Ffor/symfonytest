$(document).ready(function () {
   $("#add-news-form").submit(function (event) {
      event.preventDefault();

       var data = {};

       $(this).find('[name]').each(function (index, value) {
          var name = $(this).attr('name');
           data[name] = $(this).val();
       });

       $.ajax({
          url: "/news/operation/add",
          type: 'POST',
          data: data,
           success: function (response) {
               console.log(data);
           }
      })
   });
});